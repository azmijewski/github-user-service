create table query_user_count(
    LOGIN varchar(500) not null primary key,
    REQUEST_COUNT bigint not null default 0
);