package com.kierait.userservice.infrastucture.queryusercount;

import com.kierait.userservice.domain.queryusercount.QueryUserCountDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
class QueryUserCountDaoImpl implements QueryUserCountDao {
    private final QueryUserCountRepository queryUserCountRepository;

    @Override
    public void incrementQueryCount(String login) {
        ensureEntityIsCreated(login);
        queryUserCountRepository.incrementCountByLogin(login);
    }

    private void ensureEntityIsCreated(String login) {
        if (queryUserCountRepository.notExistByLogin(login)) {
            create(login);
        }
    }

    private void create(String login) {
        try {
            queryUserCountRepository.save(new QueryUserCountEntity(login));
        } catch (DataIntegrityViolationException ex) {
            log.debug("User with login: {} already exists", login);
        }
    }
}
