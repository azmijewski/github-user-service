package com.kierait.userservice.infrastucture.queryusercount;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
interface QueryUserCountRepository extends CrudRepository<QueryUserCountEntity, String> {

    default boolean notExistByLogin(String login) {
        return !existsById(login);
    }

    @Modifying
    @Transactional
    @Query("update QueryUserCountEntity quce " +
            "set quce.requestCount = quce.requestCount + 1 " +
            "where quce.login = :login")
    void incrementCountByLogin(@Param("login") String login);
}
