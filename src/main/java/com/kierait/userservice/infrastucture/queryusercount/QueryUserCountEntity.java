package com.kierait.userservice.infrastucture.queryusercount;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PROTECTED;

@Entity
@NoArgsConstructor(access = PROTECTED)
@Getter
@Table(name = "query_user_count")
class QueryUserCountEntity {
    @Id
    private String login;

    private long requestCount;

    public QueryUserCountEntity(String login) {
        this.login = login;
    }
}
