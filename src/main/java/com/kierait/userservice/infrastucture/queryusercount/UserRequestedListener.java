package com.kierait.userservice.infrastucture.queryusercount;

import com.kierait.userservice.domain.queryusercount.QueryUserCountService;
import com.kierait.userservice.domain.user.UserRequested;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class UserRequestedListener {
    private final QueryUserCountService queryUserCountService;

    @Async
    @EventListener(UserRequested.class)
    public void onUserRequested(UserRequested userRequested) {
        queryUserCountService.incrementQueryCount(userRequested.login());
    }
}
