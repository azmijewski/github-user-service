package com.kierait.userservice.infrastucture.user;

import com.kierait.userservice.domain.user.User;
import com.kierait.userservice.domain.user.UserDao;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
class GithubUserDao implements UserDao {
    private final GithubClient githubClient;
    private final UserMapper userMapper;

    @Override
    public Optional<User> findByLogin(String login) {
        try {
            var githubUser = githubClient.getByLogin(login);
            return Optional.of(
                    userMapper.map(githubUser)
            );
        } catch (FeignException.NotFound notFound) {
            return Optional.empty();
        } catch (FeignException.Forbidden forbidden) {
            throw new RateLimitedException();
        } catch (FeignException exception) {
            throw new GithubException(exception);
        }
    }
}
