package com.kierait.userservice.infrastucture.user;

import com.kierait.userservice.domain.exception.ApplicationException;

class GithubException extends ApplicationException {
    protected GithubException(Throwable cause) {
        super("User provider error", cause, 500, "USER_PROVIDER_ERROR");
    }
}
