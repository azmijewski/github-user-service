package com.kierait.userservice.infrastucture.user;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("user-mapper")
@Data
public class UserMapperProperties {
    private int scale;
}
