package com.kierait.userservice.infrastucture.user;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Value;

import java.math.BigDecimal;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Value
public class GithubUser{
    Long id;
    String login;
    String name;
    String type;
    String avatarUrl;
    String createdAt;
    BigDecimal publicRepos;
    BigDecimal followers;
}


