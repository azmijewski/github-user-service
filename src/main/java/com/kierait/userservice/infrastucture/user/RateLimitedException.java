package com.kierait.userservice.infrastucture.user;

import com.kierait.userservice.domain.exception.ApplicationException;

class RateLimitedException extends ApplicationException {
    protected RateLimitedException() {
        super("Too many request", 429, "TOO_MANY_REQUESTS");
    }
}
