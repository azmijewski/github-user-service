package com.kierait.userservice.infrastucture.user;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(
        name = "github-client",
        url = "${github-client.url}"
)
interface GithubClient {
    @GetMapping("/users/{login}")
    GithubUser getByLogin(@PathVariable String login);
}
