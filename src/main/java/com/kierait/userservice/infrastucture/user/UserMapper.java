package com.kierait.userservice.infrastucture.user;

import com.kierait.userservice.domain.user.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
@RequiredArgsConstructor
class UserMapper {
    private static final BigDecimal SIX = BigDecimal.valueOf(6);
    private static final BigDecimal TWO = BigDecimal.valueOf(2);

    private final UserMapperProperties userMapperProperties;

    public User map(GithubUser githubUser) {
        return new User(
                githubUser.getId(),
                githubUser.getLogin(),
                githubUser.getName(),
                githubUser.getType(),
                githubUser.getAvatarUrl(),
                githubUser.getCreatedAt(),
                mapCalculations(githubUser)
        );
    }

    private BigDecimal mapCalculations(GithubUser githubUser) {
        return BigDecimal.ZERO.equals(githubUser.getFollowers()) ? null : calculate(githubUser);
    }

    private BigDecimal calculate(GithubUser githubUser) {
        return SIX.divide(githubUser.getFollowers(), userMapperProperties.getScale(), RoundingMode.HALF_EVEN)
                .multiply(
                        TWO.add(githubUser.getPublicRepos())
                );
    }
}
