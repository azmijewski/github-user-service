package com.kierait.userservice.infrastucture.shared;

import com.kierait.userservice.domain.shared.EventPublisher;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class SpringApplicationEventPublisher implements EventPublisher {
    private final ApplicationEventPublisher applicationEventPublisher;

    @Override
    public <T> void publishEvent(T event) {
        applicationEventPublisher.publishEvent(event);
    }
}
