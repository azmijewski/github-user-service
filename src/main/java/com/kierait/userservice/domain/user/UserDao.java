package com.kierait.userservice.domain.user;

import java.util.Optional;

public interface UserDao {
    Optional<User> findByLogin(String login);
}
