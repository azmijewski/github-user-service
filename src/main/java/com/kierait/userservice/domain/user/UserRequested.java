package com.kierait.userservice.domain.user;

public record UserRequested(
        String login
) {
}
