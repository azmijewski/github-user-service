package com.kierait.userservice.domain.user;

import com.kierait.userservice.domain.exception.ApplicationException;

class UserNotFoundException extends ApplicationException {
    private static final String MESSAGE_TO_FORMAT = "User with login %s not found";

    protected UserNotFoundException(String login) {
        super(MESSAGE_TO_FORMAT.formatted(login), 404, "USER_NOT_FOUND");
    }
}
