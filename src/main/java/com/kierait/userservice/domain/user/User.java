package com.kierait.userservice.domain.user;

import java.math.BigDecimal;

public record User(
        Long id,
        String login,
        String name,
        String type,
        String avatarUrl,
        String createdAt,
        BigDecimal calculations
) {
}
