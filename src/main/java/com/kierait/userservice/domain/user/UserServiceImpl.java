package com.kierait.userservice.domain.user;

import com.kierait.userservice.config.logging.Loggable;
import com.kierait.userservice.domain.shared.EventPublisher;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Loggable
class UserServiceImpl implements UserService {
    private final UserDao userDao;
    private final EventPublisher eventPublisher;

    @Override
    public User getByLogin(String login) {
        eventPublisher.publishEvent(new UserRequested(login));
        return userDao.findByLogin(login)
                .orElseThrow(() -> new UserNotFoundException(login));
    }
}
