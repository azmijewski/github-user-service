package com.kierait.userservice.domain.user;

public interface UserService {
    /**
     * Method returns user data by his login
     * @param login user login
     * @return user
     */
    User getByLogin(String login);
}
