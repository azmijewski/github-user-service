package com.kierait.userservice.domain.shared;

public interface EventPublisher {
    <T> void publishEvent(T event);
}
