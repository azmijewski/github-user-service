package com.kierait.userservice.domain.exception;

import lombok.Getter;

@Getter
public abstract class ApplicationException extends RuntimeException{
    private final int responseCode;
    private final String errorCode;

    protected ApplicationException(String message, int responseCode, String errorCode) {
        super(message);
        this.responseCode = responseCode;
        this.errorCode = errorCode;
    }

    protected ApplicationException(String message, Throwable cause, int responseCode, String errorCode) {
        super(message, cause);
        this.responseCode = responseCode;
        this.errorCode = errorCode;
    }
}
