package com.kierait.userservice.domain.queryusercount;

public interface QueryUserCountDao {
    void incrementQueryCount(String login);
}
