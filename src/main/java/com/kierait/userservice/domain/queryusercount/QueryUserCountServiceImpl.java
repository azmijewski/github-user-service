package com.kierait.userservice.domain.queryusercount;

import com.kierait.userservice.config.logging.Loggable;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Loggable
class QueryUserCountServiceImpl implements QueryUserCountService {
    private final QueryUserCountDao queryUserCountDao;

    @Override
    public void incrementQueryCount(String login) {
        queryUserCountDao.incrementQueryCount(login);
    }
}
