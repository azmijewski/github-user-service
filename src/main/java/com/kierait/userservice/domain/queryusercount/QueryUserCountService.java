package com.kierait.userservice.domain.queryusercount;

public interface QueryUserCountService {
    /**
     * Method increment user query count
     * @param login user login
     */
    void incrementQueryCount(String login);
}
