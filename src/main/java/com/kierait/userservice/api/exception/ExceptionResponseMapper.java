package com.kierait.userservice.api.exception;

import com.kierait.userservice.domain.exception.ApplicationException;
import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
final class ExceptionResponseMapper {

    static ExceptionResponse map(ApplicationException applicationException) {
        return new ExceptionResponse(
                applicationException.getErrorCode(),
                applicationException.getMessage()
        );
    }

    static ExceptionResponse map(Exception exception) {
        return new ExceptionResponse(
                "INTERNAL_SERVER_EXCEPTION",
                exception.getMessage()
        );
    }
}
