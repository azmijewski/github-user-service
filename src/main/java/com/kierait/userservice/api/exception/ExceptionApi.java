package com.kierait.userservice.api.exception;

import com.kierait.userservice.domain.exception.ApplicationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static com.kierait.userservice.api.exception.ExceptionResponseMapper.map;

@RestControllerAdvice
@Slf4j
public class ExceptionApi {

    @ExceptionHandler
    public ResponseEntity<ExceptionResponse> onBusinessException(ApplicationException applicationException) {
       log.error("APPLICATION_EXCEPTION", applicationException);
       return ResponseEntity
               .status(applicationException.getResponseCode())
               .body(map(applicationException));
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionResponse> onException(Exception ex) {
        log.error("INTERNAL_EXCEPTION", ex);
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(map(ex));
    }
}
