package com.kierait.userservice.api.exception;

public record ExceptionResponse(
        String errorCode,
        String message
) {
}
