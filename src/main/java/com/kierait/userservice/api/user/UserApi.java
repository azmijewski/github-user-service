package com.kierait.userservice.api.user;

import com.kierait.userservice.domain.user.User;
import com.kierait.userservice.domain.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserApi {
    private final UserService userService;

    @GetMapping("/{login}")
    public User getByLogin(@PathVariable String login) {
        return userService.getByLogin(login);
    }
}
