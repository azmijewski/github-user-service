package com.kierait.userservice.config.logging;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Slf4j
class LoggingAspect {

    @Around("@within(Loggable)")
    public Object log(ProceedingJoinPoint joinPoint) throws Throwable {
        var signature = joinPoint.getSignature();
        logInput(signature, joinPoint.getArgs());
        try {
            var result = joinPoint.proceed();
            logOutput(signature, result);
            return result;
        } catch (Throwable e) {
            logError(signature, e);
            throw e;
        }
    }

    private void logInput(Signature signature, Object[] args) {
        log.debug("Start of method: {} input: {}", signature.toShortString(), args);
    }

    private void logOutput(Signature signature, Object result) {
        log.debug("Finish of method: {} output: {}", signature.toShortString(), result);
    }

    private void logError(Signature signature, Throwable throwable) {
        log.debug("Method:{} ended with error", signature.toShortString(), throwable);
    }
}
