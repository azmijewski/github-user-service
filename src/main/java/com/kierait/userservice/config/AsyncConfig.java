package com.kierait.userservice.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableAsync;

@Profile("!disable-async")
@Configuration
@EnableAsync
public class AsyncConfig {
}
