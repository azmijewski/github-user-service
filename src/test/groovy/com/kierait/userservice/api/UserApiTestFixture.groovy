package com.kierait.userservice.api

import com.kierait.userservice.api.exception.ExceptionResponse
import com.kierait.userservice.domain.user.User
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

class UserApiTestFixture {
    private static final BASE_URI = "/users"

    static final LOGIN = "testLogin"
    static final ID = 1L
    static final AVATAR_URL = "https://github.com/images/${LOGIN}.gif"
    static final TYPE = "User"
    static final NAME = "firstname lastname"
    static final PUBLIC_REPOS = 2
    static final FOLLOWERS = 6
    static final CREATED_AT = "2008-01-14T04:33:35Z"

    static MockHttpServletRequestBuilder getByLogin(String login) {
        return get("$BASE_URI/$login")
    }

    static void assertUserDataIsValid(User user) {
        assertUserDataIsValidWithCalculation(user, BigDecimal.valueOf(4))
    }

    static void assertUserDataIsValidWhenFollowersIsZero(User user) {
        assertUserDataIsValidWithCalculation(user, null)
    }

    static void assertNotFoundResponseIsValid(ExceptionResponse exceptionResponse) {
        assert exceptionResponse.errorCode() == "USER_NOT_FOUND"
        assert exceptionResponse.message() == "User with login $LOGIN not found"
    }

    static void assertGithubClientErrorIsValid(ExceptionResponse exceptionResponse) {
        assert exceptionResponse.errorCode() == "USER_PROVIDER_ERROR"
        assert exceptionResponse.message() == "User provider error"
    }

    private static void assertUserDataIsValidWithCalculation(User user, BigDecimal calculation) {
        assert user.login() == LOGIN
        assert user.id() == ID
        assert user.avatarUrl() == AVATAR_URL
        assert user.type() == TYPE
        assert user.name() == NAME
        assert user.createdAt() == CREATED_AT
        assert user.calculations() == calculation
    }
}
