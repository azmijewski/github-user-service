package com.kierait.userservice.api

import com.kierait.userservice.IntegrationSpecification
import com.kierait.userservice.api.exception.ExceptionResponse
import com.kierait.userservice.domain.user.User
import org.springframework.http.HttpStatus

import static com.kierait.userservice.api.GithubStubs.registerErrorStub
import static com.kierait.userservice.api.GithubStubs.registerNotFoundStub
import static com.kierait.userservice.api.GithubStubs.registerSuccessFoundStub
import static com.kierait.userservice.api.GithubStubs.registerSuccessFoundWith0FollowersStub
import static com.kierait.userservice.api.UserApiTestFixture.LOGIN
import static com.kierait.userservice.api.UserApiTestFixture.assertGithubClientErrorIsValid
import static com.kierait.userservice.api.UserApiTestFixture.assertNotFoundResponseIsValid
import static com.kierait.userservice.api.UserApiTestFixture.assertUserDataIsValid
import static com.kierait.userservice.api.UserApiTestFixture.assertUserDataIsValidWhenFollowersIsZero
import static com.kierait.userservice.api.UserApiTestFixture.getByLogin

class UserApiTest extends IntegrationSpecification {

    def "should get user data"() {
        given:
        def login = LOGIN
        registerSuccessFoundStub(login)

        when:
        def response = mvc.perform(getByLogin(login)).andReturn().response

        then: "response status is 200 OK"
        response.status == HttpStatus.OK.value()

        and: "returned data is valid"
        def user = objectMapper.readValue(response.contentAsString, User.class)
        assertUserDataIsValid(user)

        and: "user requested event was published"
        1 * eventPublisher.publishEvent() {
            assert it.login == login
        }
    }

    def "should get user data when followers is 0"() {
        given:
        def login = LOGIN
        registerSuccessFoundWith0FollowersStub(login)

        when:
        def response = mvc.perform(getByLogin(login)).andReturn().response

        then: "response status is 200 OK"
        response.status == HttpStatus.OK.value()

        and: "returned data is valid"
        def user = objectMapper.readValue(response.contentAsString, User.class)
        assertUserDataIsValidWhenFollowersIsZero(user)

        and: "user requested event was published"
        1 * eventPublisher.publishEvent() {
            assert it.login == login
        }
    }

    def "should return exception response data when user not found"() {
        given:
        def login = LOGIN
        registerNotFoundStub(login)

        when:
        def response = mvc.perform(getByLogin(login)).andReturn().response

        then: "response status is 404 NOT_FOUND"
        response.status == HttpStatus.NOT_FOUND.value()

        and: "returned data is valid"
        def errorResponse = objectMapper.readValue(response.contentAsString, ExceptionResponse.class)
        assertNotFoundResponseIsValid(errorResponse)
    }

    def "should return exception response when github exception"() {
        given:
        def login = LOGIN
        registerErrorStub(login)

        when:
        def response = mvc.perform(getByLogin(login)).andReturn().response

        then: "response status is 500 INTERNAL_SERVER_ERROR"
        response.status == HttpStatus.INTERNAL_SERVER_ERROR.value()

        and: "returned data is valid"
        def errorResponse = objectMapper.readValue(response.contentAsString, ExceptionResponse.class)
        assertGithubClientErrorIsValid(errorResponse)
    }
}
