package com.kierait.userservice.api

import static com.github.tomakehurst.wiremock.client.WireMock.*
import static com.kierait.userservice.api.UserApiTestFixture.AVATAR_URL
import static com.kierait.userservice.api.UserApiTestFixture.CREATED_AT
import static com.kierait.userservice.api.UserApiTestFixture.FOLLOWERS;
import static com.kierait.userservice.api.UserApiTestFixture.ID
import static com.kierait.userservice.api.UserApiTestFixture.NAME
import static com.kierait.userservice.api.UserApiTestFixture.PUBLIC_REPOS
import static com.kierait.userservice.api.UserApiTestFixture.TYPE;


class GithubStubs {
    static void registerSuccessFoundStub(String login) {
        stubFor(get(urlEqualTo("/users/$login"))
                .willReturn(
                        aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(
                                        """
                                  {
                                    "login": "$login",
                                    "id": $ID,
                                    "avatar_url": "$AVATAR_URL",
                                    "type": "$TYPE",
                                    "name": "$NAME",
                                    "public_repos": $PUBLIC_REPOS,
                                    "followers": $FOLLOWERS,
                                    "created_at": "$CREATED_AT"
                                  }
                                """
                                )
                )
        )
    }

    static void registerSuccessFoundWith0FollowersStub(String login) {
        stubFor(get(urlEqualTo("/users/$login"))
                .willReturn(
                        aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(
                                        """
                                  {
                                    "login": "$login",
                                    "id": $ID,
                                    "avatar_url": "$AVATAR_URL",
                                    "type": "$TYPE",
                                    "name": "$NAME",
                                    "public_repos": $PUBLIC_REPOS,
                                    "followers": 0,
                                    "created_at": "$CREATED_AT"
                                  }
                                  """
                                )
                )
        )
    }

    static void registerNotFoundStub(String login) {
        stubFor(get(urlEqualTo("/users/$login"))
                .willReturn(
                        aResponse()
                                .withStatus(404)
                )
        )
    }

    static void registerErrorStub(String login) {
        stubFor(get(urlEqualTo("/users/$login"))
                .willReturn(
                        aResponse()
                                .withStatus(500)
                )
        )
    }
}
