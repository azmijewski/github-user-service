package com.kierait.userservice

import com.fasterxml.jackson.databind.ObjectMapper
import com.kierait.userservice.domain.shared.EventPublisher
import org.junit.ClassRule
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.spock.Testcontainers
import spock.lang.Specification

@AutoConfigureMockMvc
@Testcontainers
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("disable-async")
@SpringBootTest
class IntegrationSpecification extends Specification {

    @ClassRule
    static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer('postgres:15.2-alpine')
            .withDatabaseName("user-service")

    static {
        postgreSQLContainer.start()
        System.setProperty("test.container.db.userName", postgreSQLContainer.getUsername())
        System.setProperty("test.container.db.password", postgreSQLContainer.getUsername())
        System.setProperty("test.container.db.jdbcUrl", postgreSQLContainer.getJdbcUrl())
    }

    @Autowired
    protected MockMvc mvc

    @Autowired
    protected ObjectMapper objectMapper

    @SpringBean
    protected EventPublisher eventPublisher = Mock()
}
