package com.kierait.userservice.infrastructure.queryusercount

import com.kierait.userservice.IntegrationSpecification
import com.kierait.userservice.domain.user.UserRequested
import com.kierait.userservice.infrastucture.queryusercount.QueryUserCountRepository
import com.kierait.userservice.infrastucture.queryusercount.UserRequestedListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.jdbc.Sql
import spock.lang.Subject
import spock.lang.Unroll

@Sql(scripts = "classpath:sql/user-requested-listener-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "classpath:sql/cleanup.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class UserRequestedListenerTest extends IntegrationSpecification {
    private static final EXISTING_LOGIN = "login1"
    private static final EXPECTED_COUNTS_FOR_EXISTING_LOGIN = 2
    private static final NON_EXISTING_LOGIN = "login2"
    private static final EXPECTED_COUNTS_FOR_NON_EXISTING_LOGIN = 1

    @Subject
    @Autowired
    private UserRequestedListener userRequestedListener

    @Autowired
    private QueryUserCountRepository queryUserCountRepository

    @Unroll
    def "should increment queries count when already queried"() {
        given:
        def event = new UserRequested(login)

        when:
        userRequestedListener.onUserRequested(event)

        then: "expected counts is valid"
        def queryUserCount = queryUserCountRepository.findById(login)
        queryUserCount.isPresent()
        queryUserCount.get().requestCount == expectedCounts

        where:
        login              | expectedCounts
        EXISTING_LOGIN     | EXPECTED_COUNTS_FOR_EXISTING_LOGIN
        NON_EXISTING_LOGIN | EXPECTED_COUNTS_FOR_NON_EXISTING_LOGIN
    }
}
